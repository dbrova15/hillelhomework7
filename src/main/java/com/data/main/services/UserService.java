package com.data.main.services;

import com.data.main.Repos.UsersRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    UsersRepo usersRepo;

    public boolean newUser(String nameUser, String passUser) {
        usersRepo.add(nameUser, passUser);
        return true;
    }
}
