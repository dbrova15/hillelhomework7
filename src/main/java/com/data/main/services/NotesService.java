package com.data.main.services;

import com.data.main.DTO.NoteAddDTO;
import com.data.main.DTO.NoteDTO;
import com.data.main.DTO.ResponseDTO;
import com.data.main.DTO.UserDTO;
import com.data.main.Entities.NoteEntity;
import com.data.main.Repos.NotesRepo;
import com.data.main.Utils.MyDTOMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class NotesService {

    @Autowired
    NotesRepo notesRepo;

    public ResponseEntity<Object> add(NoteAddDTO noteDTO) {
        long idUser = noteDTO.getUserId();
        notesRepo.add(idUser, noteDTO);
        return ResponseDTO.ok("Заметка добавлена");
    }

    public ResponseEntity<Object> remove(long idNote) {
        if (notesRepo.getOne(idNote).isEmpty()) {
            return ResponseDTO.fail("Заметка не найдена");
        } else {
            notesRepo.remove(idNote);
            return ResponseDTO.ok("Заметка удвлена");
        }
    }

    public List<NoteDTO> getAll() {
        return notesRepo.getAll();
    }

    public ResponseEntity<Object> get(long idNote) {
        List<NoteEntity> resp = notesRepo.getOne(idNote);
        if (resp.isEmpty()) {
            return ResponseDTO.fail("Заметка не найдена");
        } else {

            return ResponseDTO.ok(MyDTOMapper.noteDto(resp));
        }
    }

    public ResponseEntity<Object> update(NoteDTO noteDTO) {
        long idNote = noteDTO.getId();
        if (notesRepo.getOne(idNote).isEmpty()) {
            return ResponseDTO.fail("Заметка не найдена");
        } else {
            notesRepo.update(idNote, noteDTO);
            return ResponseDTO.ok("Заметка обновленна");
        }

    }
}
