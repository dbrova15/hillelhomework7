package com.data.main.Entities;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@Setter
@Getter
@Accessors(chain = true)
public class NoteEntity {
    private Long id;
    private String name;
    private String text;
    private LocalDateTime dete;
    private Long userId;   // внешний ключ id таблицы client

}
