package com.data.main.Entities;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Setter
@Getter
@Accessors(chain = true)
public class UsersEntity {

    private long id;
    String login;
    String password;
}
