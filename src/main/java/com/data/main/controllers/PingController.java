package com.data.main.controllers;

import com.data.main.DTO.ResponseDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PingController {
    @GetMapping(value = "ping", headers = "Accept=application/json")
    public ResponseEntity<Object> ping() {
        return ResponseDTO.ok("OK");
    }
}
