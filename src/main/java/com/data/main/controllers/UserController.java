package com.data.main.controllers;

import com.data.main.DTO.ResponseDTO;
import com.data.main.DTO.UserRegistDTO;
import com.data.main.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
    @Autowired
    UserService userService;

    //    Регистрация
    @PostMapping(value = "registration", headers = "Accept=application/json")
    public ResponseEntity<Object> registration(@RequestBody UserRegistDTO userRegistDTO) {
        String nameUser = userRegistDTO.getLogin();
        String passUser = userRegistDTO.getPassword();

        if (userService.newUser(nameUser, passUser)){
            return ResponseDTO.ok("Клиент зарегистрирован");
        } else {
            return ResponseDTO.fail("Клиент уже есть в базе данных");
        }
    }
}
