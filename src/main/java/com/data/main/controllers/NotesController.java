package com.data.main.controllers;

import com.data.main.DTO.NoteAddDTO;
import com.data.main.DTO.NoteDTO;
import com.data.main.DTO.ResponseDTO;
import com.data.main.services.NotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController(value = "notes")
public class NotesController {
    @Autowired
    NotesService notesService;

    // добавление заметки
    @PostMapping(value = "add", headers = "Accept=application/json")
    public ResponseEntity<Object> addNote(@RequestBody NoteAddDTO noteDTO) {
        return notesService.add(noteDTO);
    }

    // удаление заметки
    @GetMapping(value = "remove&id={idNote}", headers = "Accept=application/json")
    @ResponseBody
    public ResponseEntity<Object> removeNote(@PathVariable long idNote) {
        return notesService.remove(idNote);
    }

    // получение списка заметок
    @GetMapping(value = "all", headers = "Accept=application/json")
    public ResponseEntity<Object> getAllNotes(){
        return ResponseDTO.ok(notesService.getAll());
    }

    // получение заметки
    @GetMapping(value = "get&id={idNote}", headers = "Accept=application/json")
    @ResponseBody
    public ResponseEntity<Object> getOneNote(@PathVariable long idNote) {
        return notesService.get(idNote);

    }

    // обновление заметки
    @PostMapping(value = "update", headers = "Accept=application/json")
    public ResponseEntity<Object> updateNote(@RequestBody NoteDTO noteDTO) {
        return notesService.update(noteDTO);
    }
}
