package com.data.main.DTO;

import com.data.main.Entities.NoteEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDate;
import java.time.LocalDateTime;


@Data
@AllArgsConstructor
@Accessors(chain = true)
public class NoteDTO {
    long id;
    String name;
    String text;
    LocalDateTime dete;
    long userId;

    public NoteDTO(NoteEntity noteEntity) {
        id = noteEntity.getId();
        name = noteEntity.getName();
        text = noteEntity.getText();
        dete = noteEntity.getDete();
        userId = noteEntity.getId();
    }
}
