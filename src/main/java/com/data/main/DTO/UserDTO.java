package com.data.main.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@Accessors(chain = true)
public class UserDTO {
    long id;
    String login;
    String password;
}
