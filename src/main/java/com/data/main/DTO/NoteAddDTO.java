package com.data.main.DTO;
import com.data.main.Entities.NoteEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@Accessors(chain = true)
public class NoteAddDTO {
    String name;
    String text;
    long userId;

    public NoteAddDTO(NoteEntity noteEntity) {
        name = noteEntity.getName();
        text = noteEntity.getText();
        userId = noteEntity.getId();
    }
}
