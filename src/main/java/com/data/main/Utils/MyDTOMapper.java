package com.data.main.Utils;

import com.data.main.DTO.NoteDTO;
import com.data.main.Entities.NoteEntity;

import java.util.ArrayList;
import java.util.List;

public class MyDTOMapper {
    public static List<NoteDTO> noteDto(List<NoteEntity> noteEntities) {
        List<NoteDTO> noteDTOList = new ArrayList<>();

        for (NoteEntity noteEntity : noteEntities) {
            noteDTOList.add(new NoteDTO(noteEntity));
        }
        return noteDTOList;
    }
}
