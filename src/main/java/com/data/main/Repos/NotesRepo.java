package com.data.main.Repos;

import com.data.main.DTO.NoteAddDTO;
import com.data.main.DTO.NoteDTO;
import com.data.main.Entities.NoteEntity;
import com.data.main.Utils.MyDTOMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class NotesRepo {

    private static final String INSERT_NOTE = "INSERT INTO notes (name, text, user_id) VALUES (?, ?, ?)";
    private static final String SETECL_ALL = "SELECT * FROM notes";
    private static final String SELECT_USER_ID = "SELECT * FROM notes WHERE id = '%s';";
    private static final String REMOVE_NOTE = "DELETE from notes WHERE id = '%s'";
    private static final String UPDATE_NOTE = "UPDATE notes SET name = '%s', text = '%s', dete = NOW() WHERE id = '%s'";

    @Autowired
    JdbcTemplate jdbcTemplate;

    public void add(long idUser, NoteAddDTO noteDTO) {
        jdbcTemplate.update(INSERT_NOTE,  noteDTO.getName(), noteDTO.getText(), idUser);
    }

    public void remove(long idNote) {
        jdbcTemplate.update(String.format(REMOVE_NOTE, idNote));
    }

    public List<NoteDTO> getAll() {
        return MyDTOMapper.noteDto(jdbcTemplate.query(SETECL_ALL, new BeanPropertyRowMapper<>(NoteEntity.class)));
    }

    public List<NoteEntity> getOne(long idNote) {
        return jdbcTemplate.query(String.format(SELECT_USER_ID, idNote),
                new BeanPropertyRowMapper<>(NoteEntity.class));
    }

    public void update(long idNote, NoteDTO noteDTO) {
        jdbcTemplate.update(String.format(UPDATE_NOTE, noteDTO.getName(), noteDTO.getText(), idNote));
    }
}
