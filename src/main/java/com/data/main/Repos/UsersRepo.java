package com.data.main.Repos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class UsersRepo {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public void add(String nameUser, String passUser) {
        jdbcTemplate.update("INSERT INTO users (login, password) VALUES (?, ?)", nameUser, passUser);
    }
}
